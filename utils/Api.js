import Axios from "axios";
import Loading from "../components/Loading";
import NavigationService from "./NavigationService";
import { AsyncStorage, Alert } from "react-native";
import global from "../global";
// base url
Axios.defaults.baseURL = "http://grubhouse.co.uk/mobileappv2/api/";

// log request
Axios.interceptors.request.use((request) => {
  console.log("Starting Request :", request.baseURL + request.url);
  console.log("Request Data :", request.data);
  console.log("Request Header :", request.headers.Authorization);
  return request;
});

// log response
Axios.interceptors.response.use((response) => {
  console.log("Response: \n", response.status, response.data);
  return response;
});

// store auth token in storage
function StoreToken(responseData) {
  // console.log(responseData);
  AsyncStorage.setItem(global.API_TOKEN, responseData, (err) => {
    if (err) {
      console.log("an error");
      throw err;
    }
    console.log("Token Stored");
  }).catch((err) => {
    console.log("error is: " + err);
  });
}

//  get user token
export async function GetToken(data) {
  try {
    let accessToken = await AsyncStorage.getItem(global.API_TOKEN).then(
      (value) => {
        if (value) {
          return value;
        }
      }
    );

    if (!accessToken) {
      console.log("no access token");
      console.log("navigate to Auth");
      Loading.hide();
      NavigationService.navigate("Auth");
    } else {
      global.AUTHTOKEN = accessToken;

      // store user data
      AsyncStorage.setItem(global.USER_DATA, JSON.stringify(data), (err) => {
        if (err) {
          console.log("an error");
          throw err;
        }
        console.log("User Data Stored");
      }).catch((err) => {
        console.log("error is: " + err);
      });
      let userData = await AsyncStorage.getItem(global.USER_DATA).then(
        (value) => {
          if (value) {
            return value;
          }
        }
      );

      global.USER = JSON.parse(userData);

      console.log("navigate to app");
      Loading.hide();

      NavigationService.navigate("User");
    }
  } catch (error) {
    console.log("Something went wrong");
  }
}

//  store user data in storage
export async function StoreUserData(data) {
  try {
    // store user data
    AsyncStorage.setItem(global.USER_DATA, JSON.stringify(data), (err) => {
      if (err) {
        console.log("an error");
        throw err;
      }
      console.log("User Data Stored");
    }).catch((err) => {
      console.log("error is: " + err);
    });
    let userData = await AsyncStorage.getItem(global.USER_DATA).then(
      (value) => {
        if (value) {
          return value;
        }
      }
    );

    global.USER = JSON.parse(userData);
    return true;
    console.log("userdata",  global.USER)
  } catch (error) {
    console.log("Something went wrong");
  }
}

export async function GetUserData() {
  try {
    // store user data
       let userData = await AsyncStorage.getItem(global.USER_DATA).then(
      (value) => {
        if (value) {
          // global.USER = JSON.parse(value)
          return value;
        }
      }
    );
      
    
  } catch (error) {
    console.log("Something went wrong");
  }
}

// login api
export async function Login(d) {
  Loading.show();
  var data = new FormData();
  data.append("api_key", " admin@1474?");
  data.append("device_token", global.CONSTANT.DEVICETYPE);
  data.append("user_mobile", d.email);
  data.append("password", d.password);
  const DATA = Axios({
    method: "post",
    url: "customerLogin",
    data,
    validateStatus: () => {
      return true;
    },
  }).then(
    function (response) {
      if (response.data.code == 1) {
        console.log(response)
        Loading.hide();
        StoreUserData(response.data).then(()=>{
          NavigationService.navigate("UserApp");
          console.log("global data", global.USER)

        })
      } else {
        Loading.hide();
        alert(response.data.msg);
      }
    }.bind(this)
  );
  // return DATA;
}

export async function Signup(d) {
  Loading.show();


  var data = new FormData();
  data.append("account_type", d.account_type);
  data.append("api_key", " admin@1474?");
  data.append("code_version", "1");
  data.append("contact_phone", d.phoneNumber);
  data.append("cpassword", d.confirm_Password);
  data.append("date_of_birth", d.date);
  data.append("device_id", global.CONSTANT.DEVICETYPE);
  data.append("device_platform", global.CONSTANT.DEVICETYPE);
  data.append("device_uiid", global.CONSTANT.DEVICETYPE);
  data.append("email_address", d.email);
  data.append("first_name", d.userName);
  data.append("last_name", "");
  data.append("password", d.password);
  console.log(data)
  await Axios({
    method: "post",
    url: "createAccount",
    data,
    validateStatus: (status) => {
      return true; // I'm always returning true, you may want to do it depending on the status received
    },
  }).then(
    function (response) {
      console.log(response)
    }.bind(this)
  );
}

// searchByCuisine
export async function SearchCuisineById(d) {
  Loading.show();

  var data = new FormData();
  data.append("api_key", " admin@1474?");
  data.append("page", "0");
  data.append("cuisine_id", d.id);
  data.append("client_id", global.USER.details.client_info.client_id);
  const DATA = await Axios({
    method: "post",
    url: "searchCuisineById",
    data,
    validateStatus: (status) => {
      return true; // I'm always returning true, you may want to do it depending on the status received
    },
  }).then(
    // SELECT * from mt_merchant WHERE cuisine REGEXP '42'
    function (response) {
      if (response.data.code == 1) {
        console.log("cuisin search detail", response.data.details)
        //   StoreToken(response.data.data.api_token);
        //   GetToken(response.data.data);
        Loading.hide();
        if(response.data.details.resto.length>0){
          NavigationService.navigate("Combo", {
            selectedCuisineName: d.text1,
            searchResult: response.data.details,
          });
        } else {
          alert("There is no restaurants for this cuisine");
        }
        
      } else {
        // NavigationService.navigate("Combo", {
        //   selectedCuisineName: d.text1
        // });
        alert("No results");
      }
      return response.data;
    }.bind(this)
  );
  Loading.hide();
  return DATA;
}

export async function GetCategory() {
  // Loading.show();

  var data = new FormData();
  data.append("api_key", " admin@1474?");
  const DATA = await Axios({
    method: "post",
    url: "GetCategory",
    data,
    validateStatus: (status) => {
      return true; // I'm always returning true, you may want to do it depending on the status received
    },
  }).then(
    function (response) {
      console.log("get category api", response.data)
      return response.data;
    }.bind(this)
  );
      // Loading.hide()
      return DATA;
}
export async function GetCategory5() {
  // Loading.show();

  var data = new FormData();
  data.append("api_key", " admin@1474?");
  const DATA = await Axios({
    method: "post",
    url: "GetCategory5",
    data,
    validateStatus: (status) => {
      return true; 
    },
  }).then(
    function (response) {
      console.log("get category api", response.data)
      return response.data;
    }.bind(this)

  );

  Loading.hide()
  return DATA;
}
export async function GetRestaurant5() {
  // Loading.show();
  var data = new FormData();
  data.append("api_key", " admin@1474?");
  const DATA = await Axios({
    method: "post",
    url: "GetRestaurant5",
    data,
    validateStatus: (status) => {
      return true;
    },
  }).then(
    function (response) {
      return response.data;
    }.bind(this)
  );
      // Loading.hide()
      return DATA;
}

export async function searchRestByAddress(d) {
  // Loading.show();

  var data = new FormData();
  data.append("api_key", " admin@1474?");
  data.append("city", d)
  const DATA = await Axios({
    method: "post",
    url: "SearchRestByAddress",
    data,
    validateStatus: (status) => {
      return true; // I'm always returning true, you may want to do it depending on the status received
    },
  }).then(
    function (response) {
      console.log("get category api", response.data)
      return response.data;
    }.bind(this)
  );
      // Loading.hide()
      return DATA;
}

export async function GetRestaurantTrending() {
  // Loading.show();
  var data = new FormData();
  data.append("api_key", " admin@1474?");
  const DATA = await Axios({
    method: "post",
    url: "GetRestaurantTrending",
    data,
    validateStatus: (status) => {
      return true;
    },
  }).then(
    function (response) {
      return response.data;
    }.bind(this)
  );
      // Loading.hide()
      return DATA;
}
export async function GetMonthFavourList() {
  // Loading.show();
  var data = new FormData();
  data.append("api_key", " admin@1474?");
  const DATA = await Axios({
    method: "post",
    url: "GetRestaurant5",
    data,
    validateStatus: (status) => {
      return true;
    },
  }).then(
    function (response) {
      return response.data;
    }.bind(this)
  );
      // Loading.hide()
      return DATA;
}

export async function GetAllRestor() {
  // Loading.show();
  var data = new FormData();
  data.append("api_key", " admin@1474?");
  const DATA = await Axios({
    method: "post",
    url: "GetAllRestor",
    data,
    validateStatus: (status) => {
      return true;
    },
  }).then(
    function (response) {
      return response.data;
    }.bind(this)
  );
  // Loading.hide()
  return DATA;
}

export async function SearchAddress(lat, lon, client_id) {

  var data = new FormData();
  data.append("api_key", " admin@1474?");
  data.append("latitude", lat)
  data.append("longitude", lon)
  data.append("client_id", client_id)
  const DATA = await Axios({
    method: "post",
    url: "SearchAddress",
    data,
    validateStatus: (status) => {
      return true;
    },
  }).then(
    function (response) {
      
      return response.data;
    }.bind(this)
  );
  // Loading.hide()
  return DATA;
}

export async function getNearBy(location) {
  // Loading.show();
  var data = new FormData();
  data.append("api_key", " admin@1474?");
  data.append("latitude", location.latitude)
  data.append("longitude", location.longitude)
  data.append("client_id", global.USER.details.client_info.client_id)
  const DATA = await Axios({
    method: "post",
    url: "GetNearby",
    data,
    validateStatus: (status) => {
      return true;
    },
  }).then(
    function (response) {
      return response.data;
    }.bind(this)
  );
  // Loading.hide()
  return DATA;
}

export async function inserFavorite(merchant_id, liked, schAddress) {
  // Loading.show();
  var data = new FormData();
  data.append("api_key", " admin@1474?");
  data.append("merchant_id", merchant_id)
  data.append("liked", liked)
  data.append("client_id", global.USER.details.client_info.client_id)
  data.append("schAddress", schAddress)
  console.log("insert favorite data", data)
  const DATA = await Axios({
    method: "post",
    url: "InserFavorite",
    data,
    validateStatus: (status) => {
      return true;
    },
  }).then(
    function (response) {
      return response.data;
    }.bind(this)
  );
  // Loading.hide()
  return DATA;
}

export async function SearchMerchantById(merchant) {
  console.log(merchant)
  // Loading.show();
  var data = new FormData();
  data.append("api_key", " admin@1474?");
  data.append("merchant_id", merchant)
  data.append("client_id", global.USER.details.client_info.client_id)
  const DATA = await Axios({
    method: "post",
    url: "searchByMerchantId",
    data,
    validateStatus: (status) => {
      return true;
    },
  }).then(
    function (response) {
      if (response.data.code == 1) {
        console.log("cuisin search detail", response.data.details)
        //   StoreToken(response.data.data.api_token);
        //   GetToken(response.data.data);
        Loading.hide();
        console.log("search by merchant_id")
        if(response.data.details.list.length){
          NavigationService.navigate("DetailsScreen", {
            merchant: response.data.details.list[0]
          });
        } else {
          alert("No results");
        }
        
      } else {
        // NavigationService.navigate("Combo", {
        //   selectedCuisineName: d.text1
        // });
        alert("No results");
      }
      return response.data;
    }.bind(this)
  );
  // Loading.hide()
  return DATA;
}
