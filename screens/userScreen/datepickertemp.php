	<?php 
	public function actioncreateAccount()
	{
		$this->data = $_POST;		
		
		$Validator=new Validator;
		if ($this->data['password']!=$this->data['cpassword']){			
			$Validator->msg[] = $this->t("Confirm password does not match");
		}
		
		$mobileapp2_reg_email = getOptionA('mobileapp2_reg_email');
		$mobileapp2_reg_phone = getOptionA('mobileapp2_reg_phone');
		
		if(empty($mobileapp2_reg_email) && empty($mobileapp2_reg_phone)){
			$mobileapp2_reg_email=1;$mobileapp2_reg_phone=1;
		}			
		
		/*check if email address is blocked*/
		if($mobileapp2_reg_email==1){
	    	if ( FunctionsK::emailBlockedCheck($this->data['email_address'])){
	    		$Validator->msg[] = $this->t("Sorry but your email address is blocked by website admin");    		
	    	}	    
	    	if ( $resp = Yii::app()->functions->isClientExist($this->data['email_address']) ){			
			    $Validator->msg[] = $this->t("Sorry but your email address already exist in our records");
		    }
		}
    	
		if($mobileapp2_reg_phone==1){
	    	if ( FunctionsK::mobileBlockedCheck($this->data['contact_phone'])){
				$Validator->msg[] = $this->t("Sorry but your mobile number is blocked by website admin");			
			}
			$functionk=new FunctionsK();
			if ( $functionk->CheckCustomerMobile($this->data['contact_phone'])){
	        	$Validator->msg[] = $this->t("Sorry but your mobile number is already exist in our records");        	
	        }	
		}		
		
		if($Validator->validate()){
			$p = new CHtmlPurifier();			
			$params=array(
    		  'first_name'=>$p->purify($this->data['first_name']),
    		  'last_name'=>$p->purify($this->data['last_name']),    		  
    		  'password'=>md5($this->data['password']),
    		  'date_created'=>FunctionsV3::dateNow(),
    		  'last_login'=>FunctionsV3::dateNow(),
    		  'ip_address'=>$_SERVER['REMOTE_ADDR'],    		  
    		  'social_strategy'=>self::$social_strategy
    		);    	
    		
    		if($mobileapp2_reg_email==1){
    			$params['email_address'] = $p->purify($this->data['email_address']);
    		}
    		if($mobileapp2_reg_phone==1){
    			$params['contact_phone'] = trim($this->data['contact_phone']);
    		}
    		
    		if (isset($this->data['custom_field1'])){
	    		$params['custom_field1']=!empty($this->data['custom_field1'])?$this->data['custom_field1']:'';
	    	}
	    	if (isset($this->data['custom_field2'])){
	    		$params['custom_field2']=!empty($this->data['custom_field2'])?$this->data['custom_field2']:'';
	    	}
	    	
	    	//$this->data['next_step']='map_select_location';
	    		    	
    		/** send verification code */
            $verification=getOptionA('website_enabled_mobile_verification');            
	    	if ( $verification=="yes" && $mobileapp2_reg_phone==1){
	    		$code=Yii::app()->functions->generateRandomKey(5);		    		
	    		FunctionsV3::sendCustomerSMSVerification($params['contact_phone'],$code);
	    		$params['mobile_verification_code']=$code;
	    		$params['status']='pending';
	    		$this->data['next_step'] = 'verification_mobile';
	    	}	    	
	    	
	    	/*send email verification added on version 3*/
	    	$email_code=Yii::app()->functions->generateRandomKey(5);
	    	$email_verification=getOptionA('theme_enabled_email_verification');
	    	if ($email_verification==2 && $mobileapp2_reg_email==1){
	    		$params['email_verification_code']=$email_code;
	    		$params['status']='pending';
	    		FunctionsV3::sendEmailVerificationCode($params['email_address'],$email_code,$params);
	    		$this->data['next_step'] = 'verification_email';
	    	}
	    	
	    	$token = mobileWrapper::generateUniqueToken(15,$this->data['device_uiid']);
	    	$params['token']=$token;
	    		    	
	    	$DbExt=new DbExt;
	    	
	    	if ( $DbExt->insertData("{{client}}",$params)){	    		
	    		$customer_id =Yii::app()->db->getLastInsertID();	    		
	    		$this->code=1;
	    		$this->msg = $this->t("Registration successful");
	    		
	    		if ( $verification=="yes" && $mobileapp2_reg_phone==1){	    				
    				$this->msg=t("We have sent verification code to your mobile number");
    				
    				$this->data['client_id'] = $customer_id;
				    mobileWrapper::registeredDevice($this->data,'pending');
				    
    			} elseif ( $email_verification ==2 && $mobileapp2_reg_email==1 ){ 
    				$this->msg = mt("We have sent verification code to your email address");
    				
    				$this->data['client_id'] = $customer_id;
				    mobileWrapper::registeredDevice($this->data,'pending');
    			} else {    				
    				/*sent welcome email*/	
    				FunctionsV3::sendCustomerWelcomeEmail($params);
    				
    				$this->data['client_id'] = $customer_id;
				    mobileWrapper::registeredDevice($this->data);
    			}	   
    			
    			$this->details = array(
    			  //'form_next_step'=>isset($this->data['form_next_step'])?$this->data['form_next_step']:'',
    			  'next_step'=>isset($this->data['next_step'])?$this->data['next_step']:'',
    			  'customer_token'=>$token,    			  
    			  'contact_phone'=>isset($params['contact_phone'])?$params['contact_phone']:'',
    			  'email_address'=>isset($params['email_address'])?$params['email_address']:'',
    			);
	    		
    			/*POINTS PROGRAM*/	    			
	    	    if (FunctionsV3::hasModuleAddon("pointsprogram")){
	    		    PointsProgram::signupReward($customer_id);
	    	    }	    	    	    	    
	    	    
	    	} else $this->msg = $this->t("Something went wrong during processing your request. Please try again later");
	    	
		} else $this->msg = mobileWrapper::parseValidatorError($Validator->getError());
		
		$this->output();
	}